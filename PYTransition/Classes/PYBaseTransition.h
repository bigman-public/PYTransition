//
//  PYBaseTransition.h
//  Pods
//
//  Created by yunhe.lin on 2017/3/6.
//
//

#import <Foundation/Foundation.h>



typedef NS_ENUM (NSInteger, PYNavigationControllerOperation) {
    PYNavigationControllerOperationPush,
    PYNavigationControllerOperationPop
};

@interface PYBaseTransition : NSObject <UIViewControllerAnimatedTransitioning>

@end

@protocol PYBaseTransitionDataSource <NSObject>

- (PYBaseTransition *)initWithConfig:(NSDictionary *)config;

- (BOOL)canTransitionWithAnimationControllerForOperation:(PYNavigationControllerOperation)operation
                                      fromViewController:(UIViewController *)fromVC 
                                        toViewController:(UIViewController *)toVC;

@end
