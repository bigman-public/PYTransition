//
//  PYMagicFirstTransitionProtocol.h
//  Pods
//
//  Created by yunhe.lin on 2017/3/6.
//
//

#import <Foundation/Foundation.h>

@protocol PYMagicTransitionProtocol <NSObject>

- (UIView *)py_magicTargetView;

@end
