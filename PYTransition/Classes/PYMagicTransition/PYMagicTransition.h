//
//  PYMagicTransition.h
//  Pods
//
//  Created by yunhe.lin on 2017/3/6.
//
//

#import "PYBaseTransition.h"
#import "PYMagicTransitionProtocol.h"

@interface PYMagicTransition : PYBaseTransition 
<
UIViewControllerAnimatedTransitioning, 
PYBaseTransitionDataSource
>


@end
