//
//  PYMagicTransition.m
//  Pods
//
//  Created by yunhe.lin on 2017/3/6.
//
//

#import "PYMagicTransition.h"

@interface PYMagicTransition()

@property (nonatomic, assign) PYNavigationControllerOperation operation;
@property (nonatomic, assign) NSTimeInterval transitionTimeInterval;

@end

@implementation PYMagicTransition

- (PYBaseTransition *)initWithConfig:(NSDictionary *)config
{
    if (self = [super init]) {
        _operation = [config[@"operation"] integerValue];
        _transitionTimeInterval = [config[@"transitionTimeInterval"] doubleValue];
        if (_transitionTimeInterval == 0) {
            _transitionTimeInterval = 0.75;
        }
    }
    return self;
}

- (BOOL)canTransitionWithAnimationControllerForOperation:(PYNavigationControllerOperation)operation 
                                      fromViewController:(UIViewController *)fromVC 
                                        toViewController:(UIViewController *)toVC
{
    if (operation != PYNavigationControllerOperationPush) {
        return NO;
    }
    if (![fromVC conformsToProtocol:@protocol(PYMagicTransitionProtocol)] && 
        ![toVC conformsToProtocol:@protocol(PYMagicTransitionProtocol)]) {
        return NO;
    }
    id<PYMagicTransitionProtocol> fromViewController = (id<PYMagicTransitionProtocol>) fromVC;
    id<PYMagicTransitionProtocol> toViewController = (id<PYMagicTransitionProtocol>) toVC;
    UIView *targetView = [fromViewController py_magicTargetView];
    UIView *tempTargetView = [toViewController py_magicTargetView];
    if (!targetView || !tempTargetView) {
        return NO;
    }
    return YES;
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return _transitionTimeInterval;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    if (self.operation == PYNavigationControllerOperationPush) {
        [self doPushAnimateTransition:transitionContext];
        return;
    }
    [transitionContext completeTransition:YES];
}

- (void)doPushAnimateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    id<PYMagicTransitionProtocol> fromViewController = (id<PYMagicTransitionProtocol>) fromVC;
    id<PYMagicTransitionProtocol> toViewController = (id<PYMagicTransitionProtocol>) toVC;
    
    UIView *fromTargetView = [fromViewController py_magicTargetView];
    UIView *toTargetView = [toViewController py_magicTargetView];
    
    UIView *containerView = [transitionContext containerView];
    
    UIView *tempView = [self snapshot:fromTargetView];//[[fromViewController py_magicTargetView] snapshotViewAfterScreenUpdates:YES];
    tempView.frame = [fromTargetView convertRect:fromTargetView.bounds toView: containerView];

    //设置动画前的各个控件的状态
    fromTargetView.hidden = YES;
    toVC.view.alpha = 0;
    toTargetView.hidden = YES;
    //tempView 添加到containerView中，要保证在最前方，所以后添加
    [containerView addSubview:tempView];
    [containerView addSubview:toVC.view];
    //开始做动画
    [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:0.0 usingSpringWithDamping:0.55 initialSpringVelocity:1 / 0.55 options:0 animations:^{
        tempView.frame = [toTargetView convertRect:toTargetView.bounds toView:containerView];
        toVC.view.alpha = 1;
    } completion:^(BOOL finished) {
        tempView.hidden = YES;
        [tempView removeFromSuperview];
        toTargetView.hidden = NO;
        fromTargetView.hidden = NO;
        [transitionContext completeTransition:YES];
    }];
}

#pragma mark - Private method
- (UIView *)snapshot:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, YES, 0);
    [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:YES];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.frame = view.frame;
    return imageView;
}


@end
