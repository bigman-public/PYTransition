//
//  UIViewController+PYTransition.m
//  Pods
//
//  Created by yunhe.lin on 2017/3/6.
//
//

#import "UIViewController+PYTransition.h"
#import <objc/runtime.h>

const PYTransitionType PYTransitionMagic = 8801;
const PYTransitionType PYTransitionNone = 8800;

static NSString *const kPYTransitionTypeIdentifity = @"kPYTransitionTypeIdentifity";

@implementation UIViewController (PYTransition)

- (void)setTransitionType:(PYTransitionType)transitionType
{
    objc_setAssociatedObject(self, (__bridge const void *)(kPYTransitionTypeIdentifity), @(transitionType), OBJC_ASSOCIATION_RETAIN);
}

- (PYTransitionType)transitionType
{
    NSNumber *type = objc_getAssociatedObject(self, (__bridge const void *)(kPYTransitionTypeIdentifity));
    return type ? type.integerValue : PYTransitionNone;
}

@end
