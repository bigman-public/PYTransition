//
//  PYTransotionCenter.m
//  Pods
//
//  Created by yunhe.lin on 2017/3/6.
//
//

#import "PYTransotionCenter.h"
#import "PYMagicTransition.h"
#import "UIViewController+PYTransition.h"
#import "PYBaseTransition.h"

@interface  PYTransotionCenter() 

@end

@implementation PYTransotionCenter

- (void)steupTransitionViewController:(UIViewController *)viewController
{
    if (!viewController.navigationController) {
        return;
    }
    viewController.navigationController.delegate = self;
}

//- (instancetype)init
//{
//    if (self = [super init]) {
//    }
//    return self;
//}

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController 
                                  animationControllerForOperation:(UINavigationControllerOperation)operation 
                                               fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC
{
    return [self transitionForType:operation == UINavigationControllerOperationPush ? fromVC.transitionType : toVC.transitionType 
   animationControllerForOperation:operation 
                fromViewController:fromVC 
                  toViewController:toVC];
}

- (id<UIViewControllerAnimatedTransitioning>)transitionForType:(PYTransitionType)type 
                               animationControllerForOperation:(UINavigationControllerOperation)operation 
                                            fromViewController:(UIViewController *)fromVC 
                                              toViewController:(UIViewController *)toVC
{
    PYNavigationControllerOperation py_operation = (operation == UINavigationControllerOperationPush ? PYNavigationControllerOperationPush : PYNavigationControllerOperationPop);
    if (type == PYTransitionMagic) {
        return [self magicTransitionWithAnimationControllerForOperation:py_operation
                                                     fromViewController:fromVC 
                                                       toViewController:toVC];
    }
    return nil;
}


- (id<UIViewControllerAnimatedTransitioning>)magicTransitionWithAnimationControllerForOperation:(PYNavigationControllerOperation)operation
                                                                             fromViewController:(UIViewController *)fromVC 
                                                                               toViewController:(UIViewController *)toVC
{
    PYMagicTransition *magicTransition = [[PYMagicTransition alloc] initWithConfig:@{@"operation": @(operation)}];
    BOOL isReturnMagicTransition = [magicTransition canTransitionWithAnimationControllerForOperation:operation 
                                                                                  fromViewController:fromVC 
                                                                                    toViewController:toVC];
    return isReturnMagicTransition ? magicTransition : nil;
}

@end
