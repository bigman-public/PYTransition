//
//  UIViewController+PYTransition.h
//  Pods
//
//  Created by yunhe.lin on 2017/3/6.
//
//

#import <UIKit/UIKit.h>

typedef NSInteger PYTransitionType;

FOUNDATION_EXTERN PYTransitionType const PYTransitionMagic;
FOUNDATION_EXTERN PYTransitionType const PYTransitionNone;

@interface UIViewController (PYTransition)

@property (nonatomic, assign) PYTransitionType transitionType;

@end
