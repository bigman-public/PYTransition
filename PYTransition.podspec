
Pod::Spec.new do |s|

  def self.smart_version
    tag = `git describe --abbrev=0 --tags 2>/dev/null`.strip
    if $?.success? then tag else "0.0.1" end
  end

  s.name             = 'PYTransition'
  s.version          = smart_version
  s.summary          = 'PYTransition.'
  s.description      = <<-DESC
many animate transition
                       DESC

  s.homepage         = 'https://gitlab.com/bigman-public/PYTransition'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'yunhe.lin' => '1367159949@qq.com' }
  s.source           = { :git => 'https://gitlab.com/bigman-public/PYTransition.git', :tag => s.version.to_s }

  s.ios.deployment_target = '7.0'

  s.source_files = 'PYTransition/Classes/**/*.*'

  s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
