# PYTransition

[![CI Status](http://img.shields.io/travis/yunhe.lin/PYTransition.svg?style=flat)](https://travis-ci.org/yunhe.lin/PYTransition)
[![Version](https://img.shields.io/cocoapods/v/PYTransition.svg?style=flat)](http://cocoapods.org/pods/PYTransition)
[![License](https://img.shields.io/cocoapods/l/PYTransition.svg?style=flat)](http://cocoapods.org/pods/PYTransition)
[![Platform](https://img.shields.io/cocoapods/p/PYTransition.svg?style=flat)](http://cocoapods.org/pods/PYTransition)

## Example

####PYMagicTransition:
	
	Push: 
	
	fromViewController:
	
	Protocol -- PYMagicTransitionProtocol
	self.transitionCenter = [[PYTransotionCenter alloc] init];
	self.transitionType = PYTransitionMagic;
    [self.transitionCenter steupTransitionViewController:self];
	
	- (UIView *)py_magicTargetView
	{
    return self.targetView;
	}
	
	toViewController:
	
	Protocol -- PYMagicTransitionProtocol
	- (UIView *)py_magicTargetView
	{
    return self.targetView;
	}
	
## Requirements

## Installation

PYTransition is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "PYTransition"
```

## Author

yunhe.lin, 1367159949@qq.com

## License

PYTransition is available under the MIT license. See the LICENSE file for more info.
