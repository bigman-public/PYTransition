//
//  main.m
//  PYTransition
//
//  Created by yunhe.lin on 03/07/2017.
//  Copyright (c) 2017 yunhe.lin. All rights reserved.
//

@import UIKit;
#import "PYAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PYAppDelegate class]));
    }
}
