//
//  PYViewController.m
//  PYTransition
//
//  Created by yunhe.lin on 03/07/2017.
//  Copyright (c) 2017 yunhe.lin. All rights reserved.
//

#import "PYViewController.h"
#import "PYTransitionAnimate.h"
#import "PYAnimateViewController.h"

@interface PYViewController () <PYMagicTransitionProtocol>

@property (nonatomic, strong) UIView *targetView;
@property (nonatomic, strong) PYTransotionCenter *transitionCenter;

@end

@implementation PYViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.targetView];
    self.transitionType = PYTransitionMagic;
    [self.transitionCenter steupTransitionViewController:self];
    self.navigationController.edgesForExtendedLayout = UIRectEdgeNone;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - event handler 

- (void)jumpToPYAnimateViewController
{
    [self.navigationController pushViewController:[PYAnimateViewController new] animated:YES];
}

#pragma mark - PYMagicTransitionProtocol

- (UIView *)py_magicTargetView
{
    return self.targetView;
}

#pragma mark - 

- (UIView *)targetView
{
    if (!_targetView) {
        CGRect frame = CGRectMake(1, 100, 50, 50);
        _targetView = [[UIView alloc] initWithFrame:frame];
        _targetView.backgroundColor = [UIColor blueColor];
        _targetView.userInteractionEnabled = YES;
        [_targetView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jumpToPYAnimateViewController)]];
    }
    return _targetView;
}

- (PYTransotionCenter *)transitionCenter
{
    if (!_transitionCenter) {
        _transitionCenter = [[PYTransotionCenter alloc] init];
    }
    return _transitionCenter;
}

@end
