//
//  PYAppDelegate.h
//  PYTransition
//
//  Created by yunhe.lin on 03/07/2017.
//  Copyright (c) 2017 yunhe.lin. All rights reserved.
//

@import UIKit;

@interface PYAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
