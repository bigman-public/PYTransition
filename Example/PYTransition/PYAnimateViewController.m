//
//  PYAnimateViewController.m
//  PYTransition
//
//  Created by yunhe.lin on 2017/3/7.
//  Copyright © 2017年 yunhe.lin. All rights reserved.
//

#import "PYAnimateViewController.h"
#import "PYMagicTransitionProtocol.h"

@interface PYAnimateViewController () <PYMagicTransitionProtocol>

@property (nonatomic, strong) UIView *targetView;

@end

@implementation PYAnimateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.targetView];
//    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.edgesForExtendedLayout = UIRectEdgeNone;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - PYMagicTransitionProtocol

- (UIView *)py_magicTargetView
{
    return self.targetView;
}

#pragma mark - 

- (UIView *)targetView
{
    if (!_targetView) {
        CGRect frame = CGRectMake(0, 100, 200, 200);
        _targetView = [[UIView alloc] initWithFrame:frame];
        _targetView.backgroundColor = [UIColor blueColor];
    }
    return _targetView;
}

@end
